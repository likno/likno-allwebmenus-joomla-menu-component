<?php
/*
     Copyright (C) 2013  Likno Software

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
if(!defined('DS')){
define('DS',DIRECTORY_SEPARATOR);
}

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.helper' );
jimport('joomla.filesystem.file');
jimport('joomla.database.table');

$controller = JRequest::getVar('controller', '');

//require_once( JApplicationHelper::getPath( 'admin_html', 'com_allwebmenuspro' ) ); 
require_once(JPATH_COMPONENT.DS.'allwebmenuspro.html.php');

$task = JRequest::getVar( 'task', '' );

/*
switch ($controller) {
	case 'awmmenuextract':
	JSubMenuHelper::addEntry(JText::_('Generate Menu Structure Code'), 'index.php?option=com_allwebmenuspro&controller=awmmenuextract', true);
	JSubMenuHelper::addEntry(JText::_('Upload Compiled Menu ZIP File'), 'index.php?option=com_allwebmenuspro&controller=awmupload');
		switch ($task) {
			case 'submit':
				submit($option);
				break;
			case 'menuextractnext':
				HTML_allwebmenuspro::menuExtractNext($option);
				break;
			default:
				HTML_allwebmenuspro::menuExtractRoot($option);
				break;
		}
		break;
	case 'awmupload':
	JSubMenuHelper::addEntry(JText::_('Generate Menu Structure Code'), 'index.php?option=com_allwebmenuspro&controller=awmmenuextract');
	JSubMenuHelper::addEntry(JText::_('Upload Compiled Menu ZIP File'), 'index.php?option=com_allwebmenuspro&controller=awmupload', true);
		switch ($task) {
			case 'submit':
				submit($option);
				break;
			case 'uploadmenunext':
				HTML_allwebmenuspro::uploadMenuNext($option);
				break;
			default:
				HTML_allwebmenuspro::uploadMenuRoot($option);
				break;
		}
		break;
	default:
		HTML_allwebmenuspro::defaultRoot();
		break;
}
*/

HTML_allwebmenuspro::defaultRoot();

$stack = array();

function geturl($HashNo, $params)
{
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, "http://www.likno.com/addins/plugin-check.php" );
	curl_setopt( $ch, CURLOPT_POST, 1 );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    $postResult = curl_exec($ch);
    return $postResult;
}
/*
function submit($option) {
	global $stack;
	$menutype = JRequest::getVar( 'menutype', 'none', 'POST', 'STRING', JREQUEST_ALLOWHTML );
	$selectiontext = JRequest::getVar('selectionstext', array(), 'POST', 'array');
	$selectionlink = JRequest::getVar('selectionslink', array(), 'POST', 'array');
	$textopt = JRequest::getVar( 'textopt', 'none', 'POST', 'STRING', JREQUEST_ALLOWHTML );
	$linkopt = JRequest::getVar( 'linkopt', 'none', 'POST', 'STRING', JREQUEST_ALLOWHTML );
	$menutypeopt = JRequest::getVar( 'menutypeopt', 'none', 'POST', 'STRING', JREQUEST_ALLOWHTML );

	$database = null;		
	$database = JFactory::getDBO();
	$sql = "DELETE FROM #__allwebmenuspro WHERE menutype='" . $menutype . "'";		
	$database->setQuery($sql);
	$database->query();
	
	if ($textopt=="all") {
		$sql = "INSERT INTO #__allwebmenuspro VALUES ('" . $menutype . "',0,1)";		
		$database->setQuery($sql);
		$database->query();		
	} else {
		if ($textopt=="select") {
			for ($i=0;$i<count($selectiontext);$i++) {
				$sql = "INSERT INTO #__allwebmenuspro VALUES ('" . $menutype . "'," . $selectiontext[$i] . ",1)";		
				$database->setQuery($sql);
				$database->query();		
			}
		}
	}
	if ($linkopt=="all") {
		$sql = "INSERT INTO #__allwebmenuspro VALUES ('" . $menutype . "',0,2)";		
		$database->setQuery($sql);
		$database->query();		
	} else {
		if ($linkopt=="select") {
			for ($i=0;$i<count($selectionlink);$i++) {
				$sql = "INSERT INTO #__allwebmenuspro VALUES ('" . $menutype . "'," . $selectionlink[$i] . ",2)";		
				$database->setQuery($sql);
				$database->query();		
			}
		}
	}
	if ($menutypeopt=="") $menutypeopt=0;
	$sql = "INSERT INTO #__allwebmenuspro VALUES ('" . $menutype . "'," . $menutypeopt . ",3)";		
	$database->setQuery($sql);
	$database->query();		
	$xmlpath=dirname(__FILE__);
	$http_host = substr(JURI::base(),0,strpos(JURI::base(),"administrator")); 	
	$XMLdata=genMenu($menutype);
	HTML_allwebmenuspro::menuExtractFinish( $option, $XMLdata );
}
*/
//this function takes care of producing the correct links.
function AWM_getLink($row){
    $mainframeFront = JApplication::getInstance('site', array(), 'J');
	$mainframe_backup = JFactory::$application;
	JFactory::$application = $mainframeFront;
	$id = $row->id;
	$type = $row->type;
	$router = JSite::getRouter();
	$link = "";

	// Decode params
	$result = new JRegistry;
	$result->loadString($row->params);
	$iParams = $row->params = $result;
	
	switch ($type) {
		case 'separator':
			$link = $row->link;
			// No further action needed.
			break;
		case 'url':
			if ((strpos($row->link, 'index.php?') === 0) && (strpos($row->link, 'Itemid=') === false)) {
				// If this is an internal Joomla link, ensure the Itemid is set.
				$link = $row->link.'&Itemid='.$id;
			} else {
				$link = $row->link;
			}
			break;

		case 'alias':
			$link = 'index.php?Itemid='.$iParams->get('aliasoptions');
			break;
		default:
			if ($router->getMode() == JROUTER_MODE_SEF) {
				$link = 'index.php?Itemid='.$id;
			} else {
				$link = $row->link . '&Itemid='.$id;
			}
			break;
	}

	if (strcasecmp(substr($link, 0, 4), 'http') && (strpos($link, 'index.php?') !== false)) {
		$link = JRoute::_($link, false, $iParams->get('secure'));
	} else {
		$link = JRoute::_($link, false);
	}
	
	JFactory::$application  = $mainframe_backup;
	$link = str_replace(JURI::base(true),JURI::root(true),$link);
	return htmlspecialchars($link, ENT_QUOTES, 'UTF-8' );
}

function awm_buildSefRoute(&$uri , $app, $router)
{
	// Get the route
	$route = $uri->getPath();

	// Get the query data
	$query = $uri->getQuery(true);

	if (!isset($query['option'])) {
		return;
	}

	$menu	= $app->getMenu();

	/*
	 * Build the component route
	 */
	$component	= preg_replace('/[^A-Z0-9_\.-]/i', '', $query['option']);
	$tmp		= '';

	// Use the component routing handler if it exists
	$path = JPATH_SITE.DS.'components'.DS.$component.DS.'router.php';

	// Use the custom routing handler if it exists
	if (file_exists($path) && !empty($query)) {
		require_once $path;
		$function	= substr($component, 4).'BuildRoute';
		$function   = str_replace(array("-", "."), "", $function);
		$parts		= $function($query);

		// encode the route segments
		if ($component != 'com_search') {
			// Cheep fix on searches
			$parts = $router->_encodeSegments($parts);
		} else {
			// fix up search for URL
			$total = count($parts);
			for ($i = 0; $i < $total; $i++)
			{
				// urlencode twice because it is decoded once after redirect
				$parts[$i] = urlencode(urlencode(stripcslashes($parts[$i])));
			}
		}

		$result = implode('/', $parts);
		$tmp	= ($result != "") ? $result : '';
	}

	/*
	 * Build the application route
	 */
	$built = false;
	if (isset($query['Itemid']) && !empty($query['Itemid'])) {
		$item = $menu->getItem($query['Itemid']);
		if (is_object($item) && $query['option'] == $item->component) {
			if (!$item->home || $item->language!='*') {
				$tmp = !empty($tmp) ? $item->route.'/'.$tmp : $item->route;
			}
			$built = true;
		}
	}

	if (!$built) {
		$tmp = 'component/'.substr($query['option'], 4).'/'.$tmp;
	}

	if ($tmp) {
		$route .= '/'.$tmp;
	} elseif ($route=='index.php') {
		$route = '';
	}

	// Unset unneeded query information
	if (isset($item) && $query['option'] == $item->component) {
		unset($query['Itemid']);
	}
	unset($query['option']);

	//Set query again in the URI
	$uri->setQuery($query);
	$uri->setPath($route);
	return $uri;
}


function genMenu($menutype, $joomlamenu, $menuname) {
	$curver=getJoomlaVersion();
	$XMLdata="";
	$database = null;
	$database = JFactory::getDBO();
	
	$sql = "SELECT m.* FROM #__menu AS m WHERE m.menutype='" . $joomlamenu . "' AND parent_id=1 AND published=1 ORDER BY lft";
	$database->setQuery($sql);
	$rows	= $database->loadObjectList();
	if (!$database->getErrorNum()) {
	} else {
		$rows	= array();
	}		
	if (count($rows)>0) {
		$XMLdata = '<?xml version="1.0" encoding="UTF-8"?><mainmenu>';
		$XMLdata .= '<menutype>' . $menutype . '</menutype>';
		$XMLdata .= '<menuname>' . $menuname . '</menuname>';
		foreach(array_keys($rows) as $id) {
			$row =& $rows[$id];
			$id = $row->id;
			$link = AWM_getLink($row);
			$name = htmlspecialchars( $row->title, ENT_QUOTES, 'UTF-8' );
			$target = $row->browserNav;
			
			$XMLdata .= "<item><id>$id</id><name>$name</name><link>$link</link>";
//			$XMLdata .= "<excludetext>" . (($excludealltext==1 or $excludetext==1) ? 1 : 0) . "</excludetext>";
//			$XMLdata .= "<excludelink>" . (($excludealllink==1 or $excludelink==1) ? 1 : 0) . "</excludelink>";
			$XMLdata .= "<target>" . $target . "</target>";
			$XMLdata .= getSubmenus($joomlamenu,$id,$database) . "</item>";
		}
		$XMLdata.= '</mainmenu>';
	}
	return $XMLdata;
}

function getSubmenus($joomlamenu,$parent,$database) {
	$curver=getJoomlaVersion();
	$XMLdata="";
	$sql = "SELECT m.* FROM #__menu AS m WHERE m.menutype='" . $joomlamenu . "' AND parent_id='" . $parent . "' AND published=1 ORDER BY lft";
	$database->setQuery($sql);
	$rows	= $database->loadObjectList('id');
	if (!$database->getErrorNum()) {
	} else {
		$rows	= array();
	}		
	if (count($rows)>0) {
		$XMLdata = '<submenu>';
		foreach(array_keys($rows) as $id) {
			$row =& $rows[$id];
			$type = $row->type;
			$id = $row->id;
			$link = AWM_getLink($row);
			$name = htmlspecialchars( $row->title, ENT_QUOTES, 'UTF-8' );
			$target = $row->browserNav;
			
			$XMLdata.= "<item><id>$id</id><name>$name</name><link>$link</link>";
//			$XMLdata .= "<excludetext>" . (($excludealltext==1 or $excludetext==1) ? 1 : 0) . "</excludetext>";
//			$XMLdata .= "<excludelink>" . (($excludealllink==1 or $excludelink==1) ? 1 : 0) . "</excludelink>";
			$XMLdata .= "<target>" . $target . "</target>";
			$XMLdata.= getSubmenus($joomlamenu,$id,$database) . "</item>";
		}
		$XMLdata.= '</submenu>';
	}
	return $XMLdata;
}

function getJoomlaVersion() {
	$version = new JVersion;
	return substr($version->getShortVersion(),0,3);
}	

?>
