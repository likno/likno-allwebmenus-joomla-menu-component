<?php
/*
     Copyright (C) 2013  Likno Software

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if(!defined('DS')){ define('DS',DIRECTORY_SEPARATOR); }
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.helper' );
jimport('joomla.filesystem.file');

class plgSystemallwebmenuspro extends JPlugin
{
	var $_initialized = false;
	var $_instanceId = 0;
	var $_itemGroupId = 1000;
	var $_menutype="";
	var $_menuname="";
	var $_menu_genre="JS";
	var $_menu_structure="";
	var $_linkimageflag="bottom";

    function _PHP4() {
		if (version_compare( phpversion(), '5.0' ) < 0) {
			if (!$this->_initialized) {
				$this->_instanceId = rand(1000, 9999);
				$this->_initialize();
			}
			return true;
		} else {
		  return false;
		}  		
    }
	
    function __construct( & $subject, $config )
    {
		parent::__construct( $subject, $config );
		$this->_instanceId = rand(1000, 9999);
    }
	function onAfterRoute() {
		$this->_initialize();
	}

	function onPrepareContent(& $article, & $params, $page = 0) {
		$this->_PHP4();	
		$mainframe = JFactory::getApplication();
		return true;
	}
	function AWM_get_dynamic() {
		$x="";
		$database = null;		
		$database = JFactory::getDBO();
		$sql = 'SELECT count(*) as cnt FROM #__menu_types AS m, #__menu AS n WHERE m.menutype=n.menutype AND n.published=1';		
		$database->setQuery($sql);
		$rows	= $database->loadObjectList('');
		return 	$rows[0]->cnt;

		if (!$database->getErrorNum()) {
			return;
		} else {
			$rows	= array();
			return 5;
		}
	}
	
	function getSubmenus($joomlamenu,$parent,$database,$level,$ext) {
		$XMLdata="";
		$sql = "SELECT m.* FROM #__menu AS m WHERE m.menutype='" . $joomlamenu . "' AND parent_id=" . $parent . " AND published=1 AND access<=". max(JFactory::getUser()->getAuthorisedViewLevels()) ." ORDER BY lft";
		$database->setQuery($sql);
		$rows	= $database->loadObjectList('id');
		if (!$database->getErrorNum()) {
		} else {
			$rows	= array();
		}		
		if (count($rows)>0) {
			if ($this->_menu_genre=="JS") {
				if ($parent==1) $XMLdata="\nif (typeof(joomlalevel$ext)=='undefined') joomlalevel$ext=0;";
				if ($parent>1) {
					$XMLdata.="\njgroup".$this->_itemGroupId."=jitem".($this->_itemGroupId-1).'.newGroup("style='.$this->_menuname.$ext.'_"+((joomlalevel'.$ext.'+'.$level.')==0?"sub_group_style":"sub_group_plus_style")+((typeof(joomlahf_'.$this->_menuname.$ext.')=="object")?(((joomlalevel'.$ext.'+'.$level.')==0?joomlahf_'.$this->_menuname.$ext.'[0]:joomlahf_'.$this->_menuname.$ext.'[1])):"")+";groupid='.$this->_itemGroupId."\");";
					$groupID=$this->_itemGroupId;
					$this->_itemGroupId++;
				}
			} else {
				if ($parent>1) $XMLdata.="\n<ul>";
			}
			foreach(array_keys($rows) as $id) {
				$row =& $rows[$id];
				$id = $row->id;
				$awm_depth = $row->level-1;

				$link2 = $this->getLink($row);

				$name = htmlspecialchars( $row->title, ENT_QUOTES, 'UTF-8' );
				$target = $row->browserNav;
				
				if ($this->_menu_genre=="JS") {
                    $menu_image = $row->params["menu_image"];
                    $menu_image_align = $this->_linkimageflag;
                    if ($menu_image_align!='no' && $menu_image!='') $name=$name.";image0=../../../../../../../".$menu_image.";image1=../../../../../../../".$menu_image.";image2=../../../../../../../".$menu_image.";imagealign0=".$menu_image_align.";imagealign1=".$menu_image_align.";imagealign2=".$menu_image_align;

					if ($parent>1) {
						$XMLdata.="\njitem".$this->_itemGroupId."=jgroup".$groupID.'.newItem("style='.$this->_menuname.$ext.'_"+((joomlalevel'.$ext.'+'.$awm_depth.')==0?"main_item_style":((joomlalevel'.$ext.'+'.$awm_depth.')==1?"sub_item_style":"sub_item_plus_style"))+";itemid='.$this->_itemGroupId.";text0=".$name;
					} else {
						$XMLdata.="\njitem".$this->_itemGroupId."=joomlagroup_".$this->_menuname.$ext.'.newItem("style='.$this->_menuname.$ext.'_"+((joomlalevel'.$ext.'+'.$awm_depth.')==0?"main_item_style":((joomlalevel'.$ext.'+'.$awm_depth.')==1?"sub_item_style":"sub_item_plus_style"))+";itemid='.$this->_itemGroupId.";text0=".$name;
					}

					if ($link2 && $target<2) { 
						$XMLdata.=";url=".$link2;
						if ($target==1) $XMLdata.=";targetframe=new";
					}
					$XMLdata.="\");";

					if ($link2 && $target==2) {
						$XMLdata.="\njitem".$this->_itemGroupId.".script2=\"window.open('".$link2."','test','toolbar=no, status=no, scrollbars=yes, location=no, fullscreen=no, menubar=no, resizable=yes');\";";
					}
				} else {
					$XMLdata.="\n<li><a href='".$link2."'".($target==1?" target='_blank'":($target==2?" onclick='window.open(\"".$link2."\",\"test\",\"toolbar=no, status=no, scrollbars=yes, location=no, fullscreen=no, menubar=no, resizable=yes\");'":"")).">".$name."</a>";
				}
				
				$this->_itemGroupId++;
				$XMLdata .= $this->getSubmenus($joomlamenu,$id,$database,$awm_depth, $ext);
				if ($this->_menu_genre!="JS") $XMLdata.="</li>";
			}
			if ($this->_menu_genre!="JS") if ($parent>1) $XMLdata.="\n</ul>";
		}
		return $XMLdata;
	}
	
	function genMenu($joomlamenu,$database,$revs){
		$XMLdata="";
		for ($i=1; $i<=$revs; $i++) {
			$XMLdata .= $this->getSubmenus($joomlamenu,1,$database,0, ($i>1)?"_rm".$i:"");
		}
		return $XMLdata;
	}

	function onAfterRender() {
		$database = null;
		$database = JFactory::getDBO();
		$this->_PHP4();	
		$mainframe = JFactory::getApplication();
		global $Itemid;
		

		if ($mainframe->isAdmin()) return;
		
		$query = "SELECT * from #__awm_menus";
		$database->setQuery($query);
		$menurows = $database->loadObjectList();
		if (!$database->getErrorNum()) {
			$awm_total_tabs = count($menurows);
			foreach(array_keys($menurows) as $id) {
				$row =& $menurows[$id];
				if ($row->enabled==1) {		// for every menu that is enabled
					$this->_menuname = $row->menuname;
					$this->_linkimageflag = $row->linkimageflag;

					if (JFile::exists(JPATH_SITE.DS.'plugins'.DS.'system'.DS.'allwebmenuspro'.DS.'menu_files'.DS."awm".$row->menuname.DS.$row->menuname.".js")) {	// if its folder exists
						// check if module also exists, if so check if it is active and visible in the current page
						$showme = true;
						$sql = "SELECT id FROM #__modules WHERE title='AllWebMenus Pro: ".$this->_menuname."'";
						$database->setQuery($sql);
						$rows = $database->loadObjectList('');
						if (!$database->getErrorNum() && count($rows)>0) {
							$mod_id = $rows[0]->id;
							$sql = "SELECT menuid FROM #__modules_menu WHERE moduleid='$mod_id'";
							$database->setQuery($sql);
							$rows = $database->loadObjectList('');
							if (!$database->getErrorNum()) {
								$showme = count($rows)>0?($rows[0]->menuid>0?false:true):false;
								for ($i=0; $i < count($rows); $i++) {
									if (ABS($rows[$i]->menuid)==JSite::getMenu()->getActive()->id) {
										$showme = $rows[$i]->menuid>0?true:false;
									}
								}
							}
						}
						
						// if all tests passed then show the Linking Code
						if ($showme) {
							$info = $this->read_ULLI_structure($this->_menuname);
							$this->_menu_genre = $info[0];
							$this->_menu_structure = $info[1];
							$this->menu_revisions = $info[2];

							$uri = JFactory::getURI();
							$menupath = $uri->root();
							$document = JFactory :: getDocument();
							$doctype = $document->getType();
							if ($doctype == 'html' and !$mainframe->isAdmin() and JRequest::getString('tmpl', '') != 'component') {
								$body = JResponse :: getBody();
								preg_match ( "/<body([^\?>])*(<\?([^>])*\?>([^>\?])*)*>/", $body, $matches );
								if (count($matches)>0){
									$pieces = explode($matches[0],$body);
									if (count($pieces) >= 2){
										$bodyNew=$pieces[0].$matches[0];
										if ($this->_menu_genre=="JS") {
											$bodyNew.="\n<!-- ******** BEGIN ALLWEBMENUS CODE FOR ".$this->_menuname." ******** -->\n";
											$bodyNew.='<script type="text/javascript">var MenuLinkedBy="AllWebMenus [4]",awmMenuName="'.$this->_menuname.'",awmBN="908";awmAltUrl="";</script>';
											$bodyNew.='<script charset="UTF-8" src="' . $menupath .$this->_menuname. '.js' . '" type="text/javascript"></script>';
											$bodyNew.="\n".'<script type="text/javascript">'."\n".'if (typeof(Menu)!="undefined") awmBuildMenu();';
											if ($row->menutype!="Static") $bodyNew.=$this->genMenu($row->joomlamenu,$database,$this->menu_revisions);
											$bodyNew.="\n".'if (typeof('.$this->_menuname.')!="undefined") ProduceMenu('.$this->_menuname.');';
											$bodyNew.="\n".'</script>';
											$bodyNew.="\n<!-- ******** END ALLWEBMENUS CODE FOR ".$this->_menuname." ******** -->\n";
										} else {
											if ($this->_menu_genre=="ULLI") {
												$bodyNew.="\n<!-- ******** BEGIN ALLWEBMENUS CODE FOR ".$this->_menuname." ******** -->\n";
												$bodyNew.='<script type="text/javascript">var MenuLinkedBy="AllWebMenus [4]",awmMenuName="'.$this->_menuname.'",awmBN="908";awmAltUrl="";</script>';
												$bodyNew.='<script charset="UTF-8" src="' . $menupath .$this->_menuname. '.js' . '" type="text/javascript"></script>';
												$bodyNew.='<script type="text/javascript">awmBuildMenu();</script>';
												$tmp = $this->genMenu($row->joomlamenu,$database,$this->menu_revisions);
												$bodyNew.="\n<!-- ******** END ALLWEBMENUS CODE FOR ".$this->_menuname." ******** -->";
												$bodyNew.="\n".str_replace("<!--DYNAMIC STRUCTURE CODE-->",$tmp,$this->_menu_structure);
											} else {
												$bodyNew.="\n<!-- ******** BEGIN ALLWEBMENUS CODE FOR ".$this->_menuname." (CSS MENU)******** -->";
												$bodyNew.="\n".'<link href="'.$menupath.$this->_menuname.'.css" rel="stylesheet" type="text/css">';
												$bodyNew.="\n".'<script charset="UTF-8" src="'.$menupath.$this->_menuname.'.js" type="text/javascript"></script>';
												$tmp = $this->genMenu($row->joomlamenu,$database,$this->menu_revisions);
												$bodyNew.="\n<!-- ******** END ALLWEBMENUS CODE FOR ".$this->_menuname." (CSS MENU)******** -->";//.$pieces[1];
												$bodyNew.="\n".str_replace("<!--DYNAMIC STRUCTURE CODE-->",$tmp,$this->_menu_structure);
											}
										}
										$bodyNew.=$pieces[1];
										for ($i=2; $i<count($pieces); $i++) $bodyNew.=$matches[0].$pieces[$i];
										JResponse :: setBody($bodyNew);
									}
								}
							}							
							
							
							
							
							
							
							
						}
					}
				}
			}
		}
	}
	function _initialize() {
		$mainframe = JFactory::getApplication();
		if ($this->_initialized) {
			JError::raiseWarning( '1' , 'awmplugin instanceId=' . $this->_instanceId . ' was initialized already');
			return true;
		}

		$this->_initialized = true;
		$document = JFactory :: getDocument();
		$doctype = $document->getType();

		if ($doctype !== 'html') {
			return false;
		}
		$Ok = true;
		return $Ok;
	}
	
	function _getConfigValue($paramName = '', $default = '') {
		$value = $default;

		switch ($paramName) {
			case 'access' :
				if ($this->params) {
					$value = 0;
				} else {
					$value = 0; //999; 
				}
				break;
			default :
				if ($this->params) {
					$value = $this->params->get($paramName, $default);
				}
		}
		return $value;
	}

	function getLink($row){
		$mainframe = JFactory::getApplication('site');
		$id = $row->id;
		$type = $row->type;
		$router = JSite::getRouter();
		$link = "";
		// Decode params
		$result = new JRegistry;
		$result->loadString($row->params);
		$iParams = $row->params = $result;

		switch ($type)
				{
					case 'separator':
                                            $link = $row->link;
						// No further action needed.
						break;

					case 'url':
						if ((strpos($row->link, 'index.php?') === 0) && (strpos($row->link, 'Itemid=') === false)) {
							// If this is an internal Joomla link, ensure the Itemid is set.
							$link = $row->link.'&Itemid='.$id;
						}
                                                else
                                                    $link = $row->link;
						break;

					case 'alias':
                                                $link = 'index.php?Itemid='.$iParams->get('aliasoptions');
						break;
                                            case 'menulink':
                                                $link= 'index.php?Itemid='.$iParams->get('menu_item');
						break;

					default:
						if ($router->getMode() == JROUTER_MODE_SEF) {
							$link = 'index.php?Itemid='.$id;
						}
						else {
							$link = $row->link . '&Itemid='.$id;
						}
						break;
				}

		if (strcasecmp(substr($link, 0, 4), 'http') && (strpos($link, 'index.php?') !== false)) {
			$link = JRoute::_($link, false, $iParams->get('secure'));
		}
		else {
			$link = JRoute::_($link, false);
		}
		return $link;
	}

	function getJoomlaVersion() {
		$version = new JVersion;
		return substr($version->getShortVersion(),0,3);
	}
	
	function read_ULLI_structure($menuname) {
		$struct = "";
		$gen = "JS";
		$revs = "1";
		$menufilename=JPATH_SITE.DS.'plugins'.DS.'system'.DS.'allwebmenuspro'.DS.'menu_files'.DS."awm".$menuname.DS.'info.txt';
		$menufilenameexists=JFile::exists($menufilename);
		if ($menufilenameexists) {
			if ($awm_menuinfofile = fopen($menufilename, 'r')) {
				while($tmp = fgets($awm_menuinfofile, filesize($menufilename))) {
					if (substr($tmp,0,7)=="Genre: ") $gen = trim(substr($tmp,7));
					if (trim($tmp)=="***Start Structure Code***") while (trim($tmp=fgets($awm_menuinfofile, filesize($menufilename)))!="***End Structure Code***") $struct .= $tmp;
					if (substr($tmp,0,12)=="Responsive: ") $revs = substr($tmp,12);
				}
				fclose($awm_menuinfofile);
			}
		}
		return array($gen, $struct, $revs);
	}
}

