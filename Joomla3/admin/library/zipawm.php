<?php
/**
 * @version		$Id:zip.php 6961 2007-03-15 16:06:53Z tcp $
 * @package		Joomla.Framework
 * @subpackage	FileSystem
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();

/**
 * ZIP format adapter for the JArchive class
 *
 * The ZIP compression code is partially based on code from:
 *   Eric Mueller <eric@themepark.com>
 *   http://www.zend.com/codex.php?id=535&single=1
 *
 *   Deins125 <webmaster@atlant.ru>
 *   http://www.zend.com/codex.php?id=470&single=1
 *
 * The ZIP compression date code is partially based on code from
 *   Peter Listiak <mlady@users.sourceforge.net>
 *
 * This class is inspired from and draws heavily in code and concept from the Compress package of
 * The Horde Project <http://www.horde.org>
 *
 * @contributor  Chuck Hagenbuch <chuck@horde.org>
 * @contributor  Michael Slusarz <slusarz@horde.org>
 * @contributor  Michael Cochrane <mike@graftonhall.co.nz>
 *
 * @author		Louis Landry <louis.landry@joomla.org>
 * @package 	Joomla.Framework
 * @subpackage	FileSystem
 * @since		1.5
 */
class JArchiveZipAwm extends JObject
{
	var $_methods = array (
		0x0 => 'None',
		0x1 => 'Shrunk',
		0x2 => 'Super Fast',
		0x3 => 'Fast',
		0x4 => 'Normal',
		0x5 => 'Maximum',
		0x6 => 'Imploded',
		0x8 => 'Deflated'
	);
	var $_ctrlDirHeader = "\x50\x4b\x01\x02";
	var $_ctrlDirEnd = "\x50\x4b\x05\x06\x00\x00\x00\x00";
	var $_fileHeader = "\x50\x4b\x03\x04";
	var $_data = null;
	var $_metadata = null;

	function extract($archive, $destination, $options = array ())
	{
		if ( ! is_file($archive) )
		{
			$this->set('error.message', 'Archive does not exist');
			return false;
		}
		return ($this->_extract($archive, $destination, $options))? true : JError::raiseWarning(100, $this->get('error.message'));
	}

	function _extract($archive, $destination, $options)
	{
		$this->_data = null;
		$this->_metadata = null;

		if (!extension_loaded('zlib')) {
			$this->set('error.message', 'Zlib Not Supported');
			return false;
		}

		if (!$this->_data = JFile::read($archive)) {
			$this->set('error.message', 'Unable to read archive');
			return false;
		}
		if (!$this->_getZipInfo($this->_data)) {
			return false;
		}
		for ($i=0,$n=count($this->_metadata);$i<$n;$i++) {
			if (substr($this->_metadata[$i]['name'], -1, 1) != '/' && substr($this->_metadata[$i]['name'], -1, 1) != '\\') {
				$buffer = $this->_getFileData($i);
				$path = JPath::clean($destination.DS.$this->_metadata[$i]['name']);
				if (!JFolder::create(dirname($path))) {
					$this->set('error.message', 'Unable to create destination');
					return false;
				}
				$ret=@ chmod(dirname($path), octdec('0777'));				
				if (JFile::write($path, $buffer) === false) {
					$this->set('error.message', 'Unable to write entry');
					return false;
				}
				$ret=@ chmod($path, octdec('0777'));				
			}
		}
		return true;
	}

	function _getZipInfo(& $data)
	{
		// Initialize variables
		$entries = array ();

		// Get details from Central directory structure.
		$fhStart = strpos($data, $this->_ctrlDirHeader);
		do {
			if (strlen($data) < $fhStart +31) {
				$this->set('error.message', 'Invalid ZIP data');
				return false;
			}
			$info = unpack('vMethod/VTime/VCRC32/VCompressed/VUncompressed/vLength', substr($data, $fhStart +10, 20));
			$name = substr($data, $fhStart +46, $info['Length']);

			$entries[$name] = array('attr' => null, 'crc' => sprintf("%08s", dechex($info['CRC32'] )), 'csize' => $info['Compressed'], 'date' => null, '_dataStart' => null, 'name' => $name, 'method' => $this->_methods[$info['Method']], '_method' => $info['Method'], 'size' => $info['Uncompressed'], 'type' => null);
			$entries[$name]['date'] = mktime((($info['Time'] >> 11) & 0x1f), (($info['Time'] >> 5) & 0x3f), (($info['Time'] << 1) & 0x3e), (($info['Time'] >> 21) & 0x07), (($info['Time'] >> 16) & 0x1f), ((($info['Time'] >> 25) & 0x7f) + 1980));

			if (strlen($data) < $fhStart +43) {
				$this->set('error.message', 'Invalid ZIP data');
				return false;
			}
			$info = unpack('vInternal/VExternal', substr($data, $fhStart +36, 6));

			$entries[$name]['type'] = ($info['Internal'] & 0x01) ? 'text' : 'binary';
			$entries[$name]['attr'] = (($info['External'] & 0x10) ? 'D' : '-') .
									  (($info['External'] & 0x20) ? 'A' : '-') .
									  (($info['External'] & 0x03) ? 'S' : '-') .
									  (($info['External'] & 0x02) ? 'H' : '-') .
									  (($info['External'] & 0x01) ? 'R' : '-');
		} while (($fhStart = strpos($data, $this->_ctrlDirHeader, $fhStart +46)) !== false);

		// Get details from local file header.
		$fhStart = strpos($data, $this->_fileHeader);
		do {
			if (strlen($data) < $fhStart +34) {
				$this->set('error.message', 'Invalid ZIP data');
				return false;
			}
			$info = unpack('vMethod/VTime/VCRC32/VCompressed/VUncompressed/vLength/vExtraLength', substr($data, $fhStart +8, 25));
			$name = substr($data, $fhStart +30, $info['Length']);
			$entries[$name]['_dataStart'] = $fhStart +30 + $info['Length'] + $info['ExtraLength'];
		} while (strlen($data) > $fhStart +30 + $info['Length'] && ($fhStart = strpos($data, $this->_fileHeader, $fhStart +30 + $info['Length'])) !== false);

		$this->_metadata = array_values($entries);
		return true;
	}

	function _getFileData($key) {
		if ($this->_metadata[$key]['_method'] == 0x8) {
			// If zlib extention is loaded use it
			if (extension_loaded('zlib')) {
				return @ gzinflate(substr($this->_data, $this->_metadata[$key]['_dataStart'], $this->_metadata[$key]['csize']));
			}
		}
		elseif ($this->_metadata[$key]['_method'] == 0x0) {
			/* Files that aren't compressed. */
			return substr($this->_data, $this->_metadata[$key]['_dataStart'], $this->_metadata[$key]['csize']);
		} elseif ($this->_metadata[$key]['_method'] == 0x12) {
			// Is bz2 extension loaded?  If not try to load it
			if (!extension_loaded('bz2')) {
				if (JPATH_ISWIN) {
					@ dl('php_bz2.dll');
				} else {
					@ dl('bz2.so');
				}
			}
			// If bz2 extention is sucessfully loaded use it
			if (extension_loaded('bz2')) {
				return bzdecompress(substr($this->_data, $this->_metadata[$key]['_dataStart'], $this->_metadata[$key]['csize']));
			}
		}
		return '';
	}

}