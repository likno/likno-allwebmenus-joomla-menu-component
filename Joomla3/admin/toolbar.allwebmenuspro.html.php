<?php
/*
     Copyright (C) 2013  Likno Software

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access 3' );

class TOOLBAR_allwebmenuspro {
	public static function _DEFAULTROOT() {
		JToolBarHelper::title( JText::_( 'Joomla Component for the AllWebMenus Pro JavaScript/CSS menu builder' ),'component' );
	}
	public static function _DEFAULUPLOAD() {
		JToolBarHelper::title( JText::_( 'Upload AllWebMenus Pro Compiled Menu ZIP File' ),'component' );
	}
	public static function _DEFAULT() {
		JToolBarHelper::title( JText::_( 'Generate Menu Structure Code - Step 1' ),'component' );
	}
	public static function _STEPTWO() {
		JToolBarHelper::title( JText::_( 'Generate Menu Structure Code - Step 2' ),'component' );
	}
	public static function _SUBMIT() {
		JToolBarHelper::title( JText::_( 'Generate Menu Structure Code - Finished' ),'component' );
	}
}
?>