<?php
/*
     Copyright (C) 2013  Likno Software

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access 2' );

jimport('joomla.application.helper' );
jimport('joomla.filesystem.file');

$controller = JRequest::getVar('controller', '');

require_once( JApplicationHelper::getPath( 'toolbar_html' ) );
switch ($controller) {
	case 'awmmenuextract':
		switch ($task) {
			case 'menuextractnext':
				TOOLBAR_allwebmenuspro::_STEPTWO();
				break;
			case 'submit':
				TOOLBAR_allwebmenuspro::_SUBMIT();
				break;
			default:
				TOOLBAR_allwebmenuspro::_DEFAULT();
				break;
		}
		break;
	case 'awmupload':
		TOOLBAR_allwebmenuspro::_DEFAULUPLOAD();
		break;
	default:
		TOOLBAR_allwebmenuspro::_DEFAULTROOT();
		break;
}

?>