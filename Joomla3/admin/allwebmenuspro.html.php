<?php
/*
     Copyright (C) 2013  Likno Software

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if(!defined('DS')){ define('DS',DIRECTORY_SEPARATOR); }

defined('_JEXEC') or die('Direct Access to this location is not allowed.');

JToolBarHelper::title( JText::_( 'Joomla Component for the AllWebMenus Pro JavaScript/CSS menu builder (ver 1.2.6)' ),'component' );

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');
jimport('joomla.installer.helper');
jimport('joomla.installer.installer');
jimport('joomla.application.helper' );

$option = $_REQUEST['option'];

class HTML_allwebmenuspro {

var $_ctrlDirHeader = "\x50\x4b\x01\x02";
var $_fileHeader = "\x50\x4b\x03\x04";
var $_data = null;
var $_metadata = null;

public static function defaultRoot() {
	$curMenuId = JRequest::getVar('curMenuId', '');
	$currentMenu = JRequest::getVar('currentMenu', '');
	if ($currentMenu=="") $currentMenu=0;
	$task = JRequest::getVar('task', '');
	$option = JRequest::getVar('option', '');
	$action = JRequest::getVar('action', '');
	$awm_selected_tab = 0;
	$database = JFactory::getDBO();
	$query = "CREATE TABLE IF NOT EXISTS `#__awm_menus` ( `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, `menuname` varchar(100) NOT NULL, `joomlamenu` varchar(100) NOT NULL, `menutype` varchar(10) NOT NULL, `enabled` tinyint(1) NOT NULL, `linkimageflag` varchar(10) NOT NULL) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';";
	$database->setQuery($query);
	$database->query();

    $columns = $database->getTableColumns('#__awm_menus');
    if(!isset($columns['linkimageflag'])){
        $querycol = "ALTER TABLE #__awm_menus ADD linkimageflag varchar(10) NOT NULL DEFAULT 'no'";
        $database->setQuery($querycol);
        $database->query();
    }

	// check if the plugin is properly installed
	$database = JFactory::getDBO();
	$sql = "SELECT * FROM #__extensions AS a WHERE a.type='plugin' AND a.element='allwebmenuspro'";
	$database->setQuery($sql);
	$rows = $database->loadObjectList();
	if (!$database->getErrorNum()) {
	} else {
		$rows	= array();
	}
	if (count($rows)==0) {
		$pluginexists=0;
	} else {
		$pluginexists=1;
	}
	if ($pluginexists==0) {	// if not, install it now!
		$installer = JInstaller::getInstance();
		if (!$installer->install(dirname(__FILE__).DS."plugin")) {
			$result = false;
		} else {
			$result = true;
			$sql = "UPDATE #__extensions set enabled=1 WHERE name='AllWebMenus Pro' AND type='plugin'";
			$database->setQuery($sql);
			$database->query();
		}
	}

	$query = "SELECT * from #__awm_menus";
	$database->setQuery($query);
	$menurows = $database->loadObjectList();
	if (!$database->getErrorNum()) {
		$firsttimer = false;
		$awm_total_tabs = count($menurows);
		if ($awm_total_tabs==0) {	// There is no menu! Consider this the first visit and create a new default menu.
			$firsttimer = true;
			$query = "INSERT INTO #__awm_menus (menuname,joomlamenu,menutype,enabled,linkimageflag) VALUES ('menu','mainmenu','Dynamic','1','no');";
			$database->setQuery($query);
			$database->query();
		} else {	// there are menus
			if ($task=="delete") {
				$query = "DELETE FROM #__awm_menus WHERE id='$curMenuId';";
				$database->setQuery($query);
				$database->query();
			} elseif ($task=="create") {
                $i = 0;
                do {
                    $i++;
                    $query = "SELECT COUNT(*) as cnt from #__awm_menus where menuname LIKE 'menu".$i."'";
                    $database->setQuery($query);
                    $count_result = $database->loadResult();
                } while ($count_result);
				$query = "INSERT INTO #__awm_menus (menuname,joomlamenu,menutype,enabled,linkimageflag) VALUES ('menu".$i."','mainmenu','Dynamic','1','no');";
				$database->setQuery($query);
				$database->query();
			} elseif ($task=="save" || $task=="generate") {
                for ($awm_t=0; $awm_t<$awm_total_tabs; $awm_t++) {
					$query  = "UPDATE #__awm_menus SET menuname='".(JRequest::getVar('AWM_menu_name_'.$awm_t, ''))."', ";
					$query .= "joomlamenu='".(JRequest::getVar('AWM_menu_structure_'.$awm_t, ''))."', ";
					$query .= "menutype='".(JRequest::getVar('AWM_menu_type_'.$awm_t, ''))."', ";
					$query .= "enabled='".(JRequest::getVar('AWM_menu_enabled_'.$awm_t, '')==""?"0":"1")."', ";
					$query .= "linkimageflag='".(JRequest::getVar('AWM_menu_link_image_flag_'.$awm_t, ''))."' ";

					$query .= "WHERE id='".(JRequest::getVar('AWM_menu_id_'.$awm_t, ''))."';";
					$database->setQuery($query);
					$database->query();
				}
				if ($task=="generate") {
					$curJoomlaMenu = $curMenuType = $curMenuName = "";
					$sql = "SELECT * FROM #__awm_menus  WHERE id='$curMenuId';";
					$database->setQuery($sql);
					$rows = $database->loadObjectList();
					if (!$database->getErrorNum()) {
					} else {
						$rows = array();
					}

					$XMLString = "";
					if (count($rows)>0) {
						$curJoomlaMenu = $rows[0]->joomlamenu;
						$curMenuType = $rows[0]->menutype;
						$curMenuName = $rows[0]->menuname;
					}
				}
			} elseif ($task=="uploadmenu") {
				HTML_allwebmenuspro::uploadMenu( $option );
			}
		}
		$query = "SELECT * from #__awm_menus";
		$database->setQuery($query);
		$menurows = $database->loadObjectList();
		$awm_total_tabs = count($menurows);
		if ($currentMenu>=$awm_total_tabs) $currentMenu = $awm_total_tabs-1;
	} else {
		echo "<p>There was a problem with the Database connection! Please try uninstalling and re-installing the component.</p>";
		echo "<p>If the problems persist, please contuct us at support@likno.com</p>";
		return;
	}
//	$sql = "UPDATE #__modules set showtitle=0 WHERE module='mod_awm_" . $menuname . $rv . "'";

?>
<style>.whitepublish:before{ color: #F2F2F2;}</style>
<script>
	function awm_show_welcome(x) {
		if (typeof(x)=="undefined") x = document.getElementById('AWM_welcome_screen').style.display=="none";
		document.getElementById('AWM_welcome_screen').style.display=x?"block":"none";
		document.getElementById('AWM_settings_publish_screen').style.display=x?"none":"block";
		document.getElementById('AWM_welcome_title_info').style.display=x?"none":"inline-block";
	}
	function awm_show_settings() {
		document.getElementById('AWM_publish_screen').style.display="none";
		document.getElementById('AWM_settings_screen').style.display="block";
	}
	function show_awm_folder_info(t) {
		x = document.getElementById('AWM_folder_info_'+t).style.display == 'none';
		document.getElementById('AWM_folder_info_'+t).style.display = x?'':'none';
		document.getElementById('show_me_'+t).innerHTML = x?'hide me':'show me';
	}
	function awm_select_menu_type(x,t) {
		document.getElementById('AWM_menu_type_'+t+'_Dynamic_info').style.display="none";
		document.getElementById('AWM_menu_type_'+t+'_Mixed_info').style.display="none";
		document.getElementById('AWM_menu_type_'+t+'_Static_info').style.display="none";
		document.getElementById('AWM_menu_type_'+t+'_'+x+'_info').style.display="";
        //		document.getElementById('menutype-'+x).checked=true;
        awm_move_link_image_flag_section(x,t);
	}
	function awm_move_link_image_flag_section(x,t) {
        var node_flag = document.getElementById('link_image_flag_section_'+t);
        var node_mixed = document.getElementById('label_mixed_'+t);
        var node_static = document.getElementById('label_static_'+t);
        if (x=="Dynamic") node_flag.parentNode.insertBefore(node_flag, node_mixed);
        if (x=="Mixed") node_flag.parentNode.insertBefore(node_flag, node_static);
	}
    function setOption(id, value)
    {
        var selectBox = document.getElementById(id),
            options = selectBox.options;

        for (var i in options) {
            if (options[i].value == value) {
                selectBox.selectedIndex = i;
                return;
            }
        }
    }
	function generateMenuStructure() {
		document.getElementById("awm_set_task").value = "generate";
		awm_settings_form.submit();
	}
	function saveOptions() {
		document.getElementById("awm_set_task").value = "save";
		awm_settings_form.submit();
	}
	function createMenu() {
		document.getElementById("awm_set_task").value = "create";
		awm_settings_form.submit();
	}
	function deleteMenu() {
		document.getElementById("awm_set_task").value = "delete";
		awm_settings_form.submit();
	}
	function awm_set_path(x) {
		for (var i=0; i<<?php echo $awm_total_tabs; ?>; i++) if (i!=x) document.getElementById('AWM_menu_path_'+i).value = document.getElementById('AWM_menu_path_'+x).value;
	}
</script>
	<div class="help-block">
		<div onclick="awm_show_welcome();" style="cursor: pointer;">Note: The Component requires the use of "AllWebMenus Pro" commercial application (version 5.3.930+).<span id="AWM_welcome_title_info" style="display: inline-block;"><a href="javascript:void(0);">Click for more info.</a></span></div><br>
		For information and updates, please visit: <a href="http://www.likno.com/addins/joomla-menu.html">http://www.likno.com/addins/joomla-menu.html</a>
	</div>
	<br />
	<!-- START OF WELCOME SCREEN -->
	<div id="AWM_welcome_screen" class="container-fluid">
		<h3>Welcome to the Joomla Component for AllWebMenus Pro<a href="javascript:void(0);" onclick="awm_show_welcome(false);" class="btn btn-primary" style="margin-left:10px;"><i class="icon-redo" style="margin-right:4px;"></i>Move to settings </a></h3>
		<p>This Component acts as a <strong>bridge</strong> between...</p>
		<div class="span3">
			<div>
				...the <strong>AllWebMenus Pro application</strong>...<br>
				<small>a powerful windows application for creating any kind of navigation menu</small>
			</div>
			<br />
			<img class="img-polaroid" src="components/com_allwebmenuspro/images/awm5snap.png" alt="AllWebMenus Pro WYSIWYG menu builder" width="250" height="203">
			<br />
		</div>
		<div class="span1 offset1">
			<h1 class="offset4">&amp;</h1>
		</div>
		<div class="span3">
			<div>
				...your <strong>Joomla site</strong>...<br>
			</div>
			<br />
			<img class="img-polaroid" src="components/com_allwebmenuspro/images/JoomlaSite.png" style="margin-top:35px;" width="250" height="203">
		</div>
		<div class="btn-toolbar">
		  <div class="btn-group" style="margin:35px 37px;">
			<a class="btn" href="http://www.likno.com/allwebmenusinfo.html"><i class="icon-list" style="margin-right:4px;"></i> Features</a>
			<a class="btn" href="http://www.likno.com/download.html"><i class="icon-download" style="margin-right:4px;"></i>Download</a>
			<a class="btn" href="http://www.likno.com/examples.html"><i class="icon-file" style="margin-right:4px;"></i> Menu Examples</a>
			<a class="btn" href="http://www.likno.com/awmstyles.php"><i class="icon-folder" style="margin-right:4px;"></i>Menu Themes</a>
			<a class="btn" href="http://www.likno.com/awmregister.php"><i class="icon-cart" style="margin-right:4px;"></i>Purchase</a>
		  </div>
		</div>
		<h4>How?</h4>
			<div class="span5">
				<ol>
					<li style="margin-bottom:10px;">Use this component to <strong>retrieve menu items from your Joomla site.</strong></li>
					<li style="margin-bottom:10px;">
						<strong>Paste</strong> these menu items into AllWebMenus Pro and create stylish, feature-rich navigation menus based on them. Fully customize these menus with styles, behaviors, effects, designs of your choice and <a href="http://www.likno.com/allwebmenusinfo.html">many more!</a>
					</li>
					<li style="margin-bottom:10px;">
						Use this component to <strong>upload</strong> your menus (multiple menus also supported) to your Joomla site. <strong>Done!</strong>
					</li>
				</ol>
				<a href="javascript:void(0);" onclick="awm_show_welcome(false);" class="btn btn-primary offset7"><i class="icon-redo" style="margin-right:4px;"></i>Move to settings </a>
			</div>

			<div class="span4">
				<img class="img-polaroid" src="components/com_allwebmenuspro/images/JoomlaAddin.png" />
			</div>
	</div> <!-- end of div container-fluid -->
	<!-- END OF WELCOME SCREEN -->

	<!-- START OF SETTINGS/PUBLISH SCREEN -->
	<div id="AWM_settings_publish_screen" style="display: none">
<?php
	if ($task=="generate") {
		$XMLString = "";
		if ($curMenuType != "") {
			$XMLString = genMenu($curMenuType, $curJoomlaMenu, $curMenuName);
		}
?>
		<!-- START OF PUBLISH SCREEN -->
		<div id="AWM_publish_screen" class="container-fluid">
			<span><h4>Generated "Menu Structure Code":</h4></span>
			<textarea  class="span12" rows="12" id="loginfo" name="loginfo"><?php echo htmlspecialchars($XMLString, ENT_QUOTES, 'UTF-8'); ?></textarea>
			<h4 style="background-color:#0088CC;color:#FFFFFF;padding:4px;">STEP 1: Copy & Paste the above "Menu Structure Code" into AllWebMenus Pro</h4>
			<div class="row-fluid">
				<div class="span9 offset1">
					<ul>
						<li><p>Select the generated "Menu Structure Code" above and <b>copy</b> it (press <strong>Ctrl+C</strong>).</p></li>
						<li><p>Switch to the <strong>AllWebMenus Pro</strong> desktop application:</p></li>
						<ul>
							<li><p>Open the <span class="label label-info"><i>"Add-ins</i> &gt; <i>Joomla Menu</i> &gt; <i>Import/Update Menu Structure from Joomla"</i></span> form.</p></li>
							<li><p><strong>Paste</strong> the above copied "Menu Structure Code" into the import form.</p></li>
							<li>
								<p>Go to Style Editor to <strong>configure</strong> your menu appearance, behavior, etc. using the related AllWebMenus Pro properties.<br />
								<small><i>(If you use the <"Static" Menu Type> option, you may also change the menu structure & content using the Menu Editor)</i> </small></p>
							</li>
							<li><p><strong>Compile</strong> your menu using the <span class="label label-info">"Add-ins &gt; Joomla Menu &gt; Compile Joomla Menu"</span> form.</p></li>
							<li><p>Your <strong>compiled menu ZIP file</strong> will be created. Now proceed to "Step 2" below to upload this ZIP file to your site.</p></li>
						</ul>
					<ul>
				</div>
			</div>
			<h4 style="background-color:#0088CC;color:#FFFFFF;padding:4px;">STEP 2: Upload the compiled menu ZIP file (produced by AllWebMenus Pro)</h4>
			<div class="container-fluid">
				<div class="span9 offset1">
					<form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="adminForm" method="post" name="adminForm">
						<fieldset>
							<label for="imenutype">
								<strong>Browse</strong> for the ZIP file that has been created by AllWebMenus Pro when you compiled your menu using the "Joomla Menu" Add-in and click the <strong>"Upload Menu ZIP File &amp; Install"</strong> button.
							</label>
							<br />
							<input id="install_package" name="install_package" type="file" />
							<!--<input class="btn btn-primary" name="submit" type="Submit" value="Upload Menu ZIP File &amp; Install" /> -->

							<button class="btn btn-primary" name="submit" type="Submit"><i class="icon-upload" style="margin-right:4px;"></i>Upload Menu ZIP File &amp; Install</button>
							<hr />
							<div class="alert alert-info">
								<strong>Notes:</strong><br />Use the ZIP file as it has been created by the AllWebMenus Pro Joomla Menu Add-in.<br />Do not change its filename, otherwise you may have problems.
							</div>
						</fieldset>
						<input type="hidden" name="option" value="<?php echo $option; ?>" />
						<input type="hidden" name="task" value="uploadmenu" />
					</form>

				</div> <!-- end of DIV SPAN9 -->
			</div> <!-- end of DIV CONTAINER-FLUID -->
			<button type="button" class="btn btn-warning" onclick="awm_show_settings();"><i class="icon-undo" style="padding-right:4px;"></i>Go Back to Settings</button>
		</div> <!-- end of DIV CONTAINER-FLUID -->
		<!-- END OF PUBLISH SCREEN -->
<?php } ?>

		<!-- START OF SETTINGS SCREEN -->
		<div id="AWM_settings_screen" class="container-fluid" style="<?php if ($task=="generate") echo "display: none;"; ?>">
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" id="awm_settings_form" method="post" name="awm_settings_form">
				<div class="form-actions">
					<div class="span6 pull-left">
						<button onclick="saveOptions();" class="btn" type="Button"><i class="icon-save-new" style="padding-right:4px;"></i>Save Settings </button>
						<button onclick="generateMenuStructure();" class="btn btn-success"><i class="icon-publish whitepublish" style="padding-right:4px;"></i>Generate Menu & Upload (also saves changes in settings)</button>
					</div>
					<div class="span6" style="text-align:right;">
						<button class="btn" type="Button" onclick="createMenu();"> <i class="icon-new" style="padding-right:4px;"></i> Create Additional Menu </button>
						<button <?php if ($awm_total_tabs == 1) echo "disabled='disabled'"; ?> class="btn" type="Button" onclick="deleteMenu();"> <i class="icon-trash" style="padding-right:4px;"></i> Delete Selected Menu </button>
					</div>
				</div>
				<div class="tabbable"> <!-- Only required for left/right tabs -->
				<ul class="nav nav-tabs" style="margin-bottom:0px;">
<?php
	for ($awm_t=0; $awm_t<$awm_total_tabs; $awm_t++) {
		echo "\t\t\t<li".(($awm_selected_tab==$awm_t)?" class='active'":"")."><a class='tabheader' id='tabheader$awm_t' href='#tab".$awm_t."' data-toggle='tab' style='color: ".($menurows[$awm_t]->enabled=="1"?"#009900":"#990000")."; outline-color:transparent;'>".$menurows[$awm_t]->menuname."</a></li>\n";
	}
?>
				</ul>
				<div class="tab-content" style="border:1px solid #DDDDDD; border-top:none;">
<?php
	for ($awm_t=0; $awm_t<$awm_total_tabs; $awm_t++) {
?>					<!-- start of tab content <?php echo $awm_t;?> -->
					<div class="tab-pane active" id="tab<?php echo $awm_t; ?>">
						<div class="container-fluid">
							<fieldset>
								<br />
								<input type="hidden" name="AWM_menu_id_<?php echo $awm_t;?>" id="AWM_menu_id_<?php echo $awm_t;?>" value="<?php echo $menurows[$awm_t]->id; ?>">
								<label class="checkbox">
									<input type="checkbox" name="AWM_menu_enabled_<?php echo $awm_t;?>" id="AWM_menu_enabled_<?php echo $awm_t;?>" <?php if ($menurows[$awm_t]->enabled=="1") echo "checked='yes'"; ?>>Show "<?php echo $menurows[$awm_t]->menuname; ?>" in site <span style="color:green;"><i>(this menu will appear in your site)</i></span>
								</label>
								<span> <strong>Menu name: </strong></span>
								<input class="input-block-level span3" name="AWM_menu_name_<?php echo $awm_t;?>" id="AWM_menu_name_<?php echo $awm_t;?>" type="text" placeholder="Put the 'Compiled Menu Name' here..." value="<?php echo $menurows[$awm_t]->menuname; ?>" onChange="javascript:this.value=this.value.toLowerCase();this.value=this.value.split(' ').join('');">
                                <span class="alert alert-info">
                                    <i class="icon-asterisk"></i>
                                    Use <b>lower case</b>, <b>without whitespaces</b>.
                                </span>
								<br /><br/>
								<h4 style="background-color:#0088CC;color:#FFFFFF;padding:4px;">Menu Structure</h4>
								<span><strong>Menu Structure:</strong></span>
								<select name="AWM_menu_structure_<?php echo $awm_t;?>">
<?php
	$sql = 'SELECT distinct m.* FROM #__menu_types AS m, #__menu AS n WHERE m.menutype=n.menutype AND n.published=1';
	$database->setQuery($sql);
	$rows = $database->loadObjectList('id');
	if (!$database->getErrorNum()) {
	} else {
		$rows = array();
	}
	foreach(array_keys($rows) as $id) {
		$row =& $rows[$id];
		$menutype = $row->menutype;
		echo "<option value=\"".$menutype."\"";
		if ($menutype==$menurows[$awm_t]->joomlamenu) echo " selected=\"selected\"";
		echo ">".$menutype."</option>";
	}
?>
								</select>
								<span class="help-block">Please select the Joomla menu you wish to generate the Menu Structure Code for.</span>
								<br />
							</fieldset>
							<fieldset>
								<h4 style="background-color:#0088CC;color:#FFFFFF;padding:4px;">Menu Type</h4>
								<span>Please select how you want your menu to behave:</span>
								<br /><br />
								<label id="label_dynamic_<?php echo $awm_t;?>" class="radio" for="AWM_menutype_dynamic_<?php echo $awm_t;?>">
									<input id="AWM_menutype_dynamic_<?php echo $awm_t;?>" type="radio" name="AWM_menu_type_<?php echo $awm_t;?>" value="Dynamic" onclick="awm_select_menu_type('Dynamic',<?php echo $awm_t;?>);"<?php if($menurows[$awm_t]->menutype=="Dynamic") echo ' checked="checked"'; ?> /><strong>"Dynamic"</strong> Menu Type
								</label>

                                <div id="link_image_flag_section_<?php echo $awm_t;?>" style="margin-left: 25px;">Use the "Link Image" option? <span style="color:green;"><i>(found at the "Link Type" tab of your menu items at Joomla's Menu Manager)</i></span>
                                    <br/>
                                <select id="AWM_menu_link_image_flag_<?php echo $awm_t;?>" name="AWM_menu_link_image_flag_<?php echo $awm_t;?>">

                                <option value="no">No, ignore it (default)</option>
                                <option value="left">Yes - Left of Text</option>
                                <option value="top">Yes - Top of Text</option>
                                <option value="right">Yes - Right of Text</option>
                                <option value="bottom">Yes - Bottom of Text</option>
                                </select>
                                </div>

                                <label id="label_mixed_<?php echo $awm_t;?>" class="radio" for="AWM_menutype_mixed_<?php echo $awm_t;?>">
									<input id="AWM_menutype_mixed_<?php echo $awm_t;?>" type="radio" name="AWM_menu_type_<?php echo $awm_t;?>" value="Mixed" onclick="awm_select_menu_type('Mixed',<?php echo $awm_t;?>);"<?php if($menurows[$awm_t]->menutype=="Mixed") echo ' checked="checked"'; ?> /><strong>"Mixed"</strong> Menu Type
								</label>
								<label id="label_static_<?php echo $awm_t;?>" class="radio" for="AWM_menutype_static_<?php echo $awm_t;?>">
									<input id="AWM_menutype_static_<?php echo $awm_t;?>" type="radio" name="AWM_menu_type_<?php echo $awm_t;?>" value="Static" onclick="awm_select_menu_type('Static',<?php echo $awm_t;?>);"<?php if($menurows[$awm_t]->menutype=="Static") echo ' checked="checked"'; ?> /><strong>"Static"</strong> Menu Type
								</label>

                                <script>setOption('AWM_menu_link_image_flag_<?php echo $awm_t;?>', '<?php echo $menurows[$awm_t]->linkimageflag; ?>');
                                    <?php
                                    if($menurows[$awm_t]->menutype=="Dynamic") echo "awm_move_link_image_flag_section('Dynamic',$awm_t)";
                                    if($menurows[$awm_t]->menutype=="Mixed") echo "awm_move_link_image_flag_section('Mixed',$awm_t)";
                                    ?>
                                </script>


                                <br />
								<div class="alert alert-info" style="<?php if($menurows[$awm_t]->menutype<>"Dynamic") echo 'display:none'; ?>" id="AWM_menu_type_<?php echo $awm_t;?>_Dynamic_info">
									<div class="container-fluid">
										<div class="span1" style="margin-right:-40px;">
											<i class="icon-help"></i>
										</div>
										<div class="span11">
											<p>You have selected to create a menu structure of <strong>"Dynamic Type"</strong>.</p>
											<p>This means that the menu items in AllWebMenus Pro will only be used for preview/styling purposes.</p>
											<p>In your actual pages these items will be ignored and the menu will be populated "dynamically" using the items of the menu specified at the "Menu Structure" option.</p>
											<p>The styles in AllWebMenus Pro Style Editor will be used to form the actual menu items.</p>
										</div>
									</div>
								</div>
								<div class="alert alert-info" style="<?php if($menurows[$awm_t]->menutype<>"Mixed") echo 'display:none'; ?>" id="AWM_menu_type_<?php echo $awm_t;?>_Mixed_info">
									<div class="container-fluid">
										<div class="span1" style="margin-right:-40px;">
											<i class="icon-help" style="margin-right:4px;"></i>
										</div>
										<div class="span11">
											<p>You have selected to create a menu structure of <strong>"Mixed Type"</strong>.</p>
											<p>This means that your menu will contain both the items you create within AllWebMenus Pro ("static") and the items you import from Joomla ("dynamic").</p>
											<p>The imported Joomla items will use the styles of the AllWebMenus Pro Style Editor but their actual content will be populated "dynamically" using the items of the menu specified at the "Menu Structure" option.</p>
											<p>The static items you create within AllWebMenus Pro will be shown as is.</p>
										</div>
									</div>
								</div>
								<div class="alert alert-info" style="<?php if($menurows[$awm_t]->menutype<>"Static") echo 'display:none'; ?>" id="AWM_menu_type_<?php echo $awm_t;?>_Static_info">
									<div class="container-fluid">
										<div class="span1" style="margin-right:-40px;">
											<i class="icon-help"></i>
										</div>
										<div class="span11">
											You have selected to create a menu structure of <strong>"Static Type"</strong>.</p>
											<p>Your menu will be edited (addition/removal/customization of items) within AllWebMenus Pro only.</p>
											<p>Any changes on your online pages will not affect its items until you perform the "Save settings &amp; Generate Menu Structure Code" action and <strong>re-import</strong> to AllWebMenus Pro.</p>
											<p>This allows for maximum customization, as your online menu will show all items and styles customized within AllWebMenus Pro.</p>
										</div>
									</div>
								</div>
							</fieldset>
							<label class="label label-info"><strong>Note:</strong> Always click &quot;Save Settings&quot; button below to apply your changes. If you leave this page without saving you will lose your unsaved changes.</label>
							<div class="form-actions">
								<div class="span5 pull-left">
									<button type="reset" onclick="ResetForm();" class="btn" ><i class="icon-refresh" style="padding-right:4px;"></i>Reset</button>
								</div>
								<div class="span7" style="text-align:right;">
									<button onclick="saveOptions();" class="btn" type="Button"><i class="icon-save-new" style="padding-right:4px;"></i> Save Settings </button>
									<button onclick="generateMenuStructure();" class="btn btn-success"><i class="icon-publish whitepublish" style="padding-right:4px;"></i>Generate Menu & Upload (also saves changes in settings)</button>
								</div>
							</div>
						</div>
					</div>
					<!-- end of tab content <?php echo $awm_t;?> -->
<?php } ?>
				</div><!-- end of tab contents -->
				<input type="hidden" name="theaction" value="" />
			</div>
			<!-- END OF SETTINGS SCREEN -->
		</div>
		<!-- END OF SETTINGS/PUBLISH SCREEN -->

		<input type="hidden" id="currentMenu" name="currentMenu" value="<?php echo $currentMenu; ?>" />
		<input type="hidden" id="curMenuId" name="curMenuId" value="<?php echo $curMenuId; ?>" />
		<input type="hidden" name="menutype" value="<?php echo $menutype; ?>" />
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="task" value="" id="awm_set_task" />

		<script type="text/javascript">
			awm_show_welcome(<?php echo $firsttimer?"true":"false"; ?>);
			jQuery('.tabheader').click(function (e) {	// change current tab
				var c = jQuery(this).attr("id").substr(9);
				selectTab(c);
			});
			function ResetForm() {
				// reset the form
				return true;
			}
			function selectTab(x) {
				jQuery('#tabheader'+(x==0?1:0)).tab('show');	// Select another tab
				jQuery('#tabheader'+x).tab('show');	// Select proper tab
				jQuery("#currentMenu").val(x);
				jQuery("#curMenuId").val(jQuery("#AWM_menu_id_"+x).val());
			}
			selectTab(<?php echo $currentMenu; ?>);
		</script>

	</form>
</div>

<?php
}

public static function uploadMenu($option) {
	$mainframe = JFactory::getApplication();
	$userfile = JRequest::getVar( 'install_package', null, 'files', 'array' );
	$filesize=$userfile['size'];
	$filename=$userfile['name'];
	$filetmpname=$userfile['tmp_name'];
	$fileerror=$userfile['error'];
	$menuname="";

	$error_occured=0;

	if ($filesize==0 && $error_occured==0) {
		$mainframe->enqueueMessage( "Error when uploading AllWebMenus Pro Compiled Menu ZIP File:" , 'error' );
		$error_occured=1;
	}

	if (substr(strtolower($filename),0,3)!="awm" && strlen($filename)>7 && $error_occured==0) {
		$mainframe->enqueueMessage( "Error when uploading AllWebMenus Pro Compiled Menu ZIP File:" , 'error' );
		$error_occured=2;
	} else {
		$menuname=substr($filename,3,strlen($filename)-7);
	}

	if ($menuname=="" && $error_occured==0) {
		$mainframe->enqueueMessage( "Error when uploading AllWebMenus Pro Compiled Menu ZIP File:" , 'error' );
		$error_occured=3;
	}

	// no error occured so continue with the menu installation
	if ($error_occured==0) {
		// check if any positioning modules already exist for this menu
		$database = JFactory::getDBO();
		for ($i=1; $i<=4; $i++) {
			$rv = ($i>1)?"_rm".$i:"";

			$sql = "SELECT * FROM #__modules AS a WHERE a.module='mod_awm_" . $menuname . $rv . "'";
			$database->setQuery($sql);
			$rows	= $database->loadObjectList();
			if (!$database->getErrorNum()) {
			} else {
				$rows	= array();
			}
			if (count($rows)==0) {
				$moduleexists[$i]=0;
			} else {
				$moduleexists[$i]=1;
			}
		}

		if (!(bool) ini_get('file_uploads')) {
			$mainframe->enqueueMessage( "No file upload can be done!" , 'error' );
			return false;
		}
		if (!extension_loaded('zlib')) {
			$mainframe->enqueueMessage( "No zlib extension found!" , 'error' );
			return false;
		}
		if (!is_array($userfile) ) {
			$mainframe->enqueueMessage( "No file selected!" , 'error' );
			return false;
		}
		if ( $userfile['error'] || $userfile['size'] < 1 )
		{
			$mainframe->enqueueMessage( "No file or empty file has been selected!" , 'error' );
			return false;
		}
		$config = JFactory::getConfig();
		$tmp_dest = $config->get('tmp_path').DS.$userfile['name'];
		$tmp_src = $userfile['tmp_name'];
		if ($tmp_dest!=$tmp_src) {
			$uploaded = JFile::upload($tmp_src, $tmp_dest,false,true);
			if (!$uploaded) {
				$mainframe->enqueueMessage( "AllWebMenus Pro Component cannot upload the following file" , 'error' );
				$mainframe->enqueueMessage( $tmp_src , 'error' );
				$mainframe->enqueueMessage( "to the following destination"  , 'error' );
				$mainframe->enqueueMessage( $tmp_dest , 'error' );
				return;
			}
		}
		$ret=@ chmod($tmp_dest, octdec('0777'));
		$package = HTML_allwebmenuspro::unpack($tmp_dest);
		if (!$package) {
			$mainframe->enqueueMessage( "AllWebMenus Pro Component cannot unpack the following file:" , 'error' );
			$mainframe->enqueueMessage( $tmp_dest , 'error' );
			return;
		}

		$filepath=JPATH_SITE.DS.'plugins'.DS.'system'.DS.'allwebmenuspro'.DS.'menu_files'.DS."awm".$menuname;
		$filesCopied = JFolder::copy($package['extractdir'].DS.'www'.DS.'awm'.$menuname,$filepath,'',true);

//		$filesCopied = JFolder::copy($package['extractdir'].DS.'www'.DS.'awm'.$menuname,JPATH_SITE.DS.'plugins'.DS.'system'.DS.'awm'.$menuname,'',true);
//		$copies2 = JFile::copy(JPATH_SITE.DS.'plugins'.DS.'system'.DS.'awm'.$menuname.DS.$menuname.'.js',JPATH_SITE.DS.$menuname.'.js');


		for ($i=1; $i<=4; $i++) {
			$rv = ($i>1)?"_rm".$i:"";
			$newfolder=$package['dir'].DS."mod".$rv;
			$modulefileexists=JFile::exists($newfolder.DS."index.html");
			if ($modulefileexists) {
				if (JFile::exists($package['extractdir'].DS.'mod'.$rv.DS.'mod_awm_'.$menuname.$rv.'.php')) {
					if ($moduleexists[$i]==0) {
						// fix the deprecated element in the XML
						$menufilename = $newfolder.DS.'mod_awm_'.$menuname.$rv.'.xml';
						$menufile = fopen($menufilename, 'r');
						$mfile = fread($menufile, filesize($menufilename));
						fclose($menufile);
						$menufile = fopen($menufilename, 'w');
						$mfile = str_replace("<install","<extension",$mfile);
						$mfile = str_replace("</install","</extension",$mfile);
						fwrite($menufile, $mfile);
						fclose($menufile);

						$installer = JInstaller::getInstance();
						if (!$installer->install($newfolder)) {
							$result = false;
						} else {
							$sql = "UPDATE #__modules set published=1, showtitle=0 WHERE module='mod_awm_" . $menuname . $rv . "'";
							$database->setQuery($sql);
							$database->query();
							$result = true;
						}
					} else {
						$filesCopied = $filesCopied && JFile::copy($package['extractdir'].DS.'mod'.$rv.DS.'mod_awm_'.$menuname.$rv.'.php',JPATH_SITE.DS.'modules'.DS.'mod_awm_'.$menuname.$rv.DS.'mod_awm_'.$menuname.$rv.'.php');
					}
				}
			}
		}

		if (!is_file($package['packagefile'])) {
			$config = JFactory::getConfig();
			$package['packagefile'] = $config->get('config.tmp_path').DS.$package['packagefile'];
		}
		HTML_allwebmenuspro::cleanupInstall($package['packagefile'], $package['extractdir']);

		if (!$filesCopied) {
			$mainframe->enqueueMessage( "AllWebMenus Pro Component could not copy file" , 'error' );
			$mainframe->enqueueMessage( $filepath.DS.$menuname.'.js', 'error' );
			$mainframe->enqueueMessage( "Please copy manually the above file to directory", 'error' );
			$mainframe->enqueueMessage( JPATH_SITE.DS, 'error' );
			$mainframe->enqueueMessage( "or change permissions of the above directory to 777 and try again.", 'error' );
		}

		$menufilename=$filepath.DS.$menuname.'.js';
		$menufilenameexists=JFile::exists($menufilename);

		if ($menufilenameexists) {
			$menufile = fopen($menufilename, 'r');
			$mfile = fread($menufile, filesize($menufilename));
			fclose($menufile);
			$bNo = explode('awmLibraryBuild=', $mfile);
			$bNo = explode(';', $bNo[1]);
			$buildNo = $bNo[0];
			$hNo = explode('awmHash=\'', $mfile);
			$hNo = explode('\'', $hNo[1]);
			$HashNo = $hNo[0];
			$params = "build=$buildNo&plugin=joomla&hash=$HashNo&rand=". rand(1,10000) ."&domain=". JURI::base();
			$menufile = fopen($menufilename, 'w');
			fwrite($menufile, str_replace("/plugins/system/awm".$menuname."/","/plugins/system/allwebmenuspro/menu_files/awm".$menuname."/",$mfile));
//			echo "replace:<br>"."/plugins/system/awm".$menuname."/"."<br>with:<br>"."/plugins/system/allwebmenuspro/menu_files/awm".$menuname."/"."<br>";
//			fwrite($menufile, $mfile);
			fclose($menufile);
			JFile::copy($menufilename,JPATH_SITE.DS.$menuname.".js");
		}

		$AWM_Text="";
		if (function_exists('curl_init') && $menufilenameexists) {
			$awm_tmp = geturl($HashNo, $params);
			if (strlen($awm_tmp) > 2 ) {
				$AWM_Text = "<br /><div class='alert alert-info'><div class='container'><div class='span1' style='margin-right:-50px;'><i class='icon-warning'></i></div><div class='span5'>" . $awm_tmp;
			}
		}
		echo $AWM_Text . '</div></div></div>';
        if ($filesCopied) {
            $msg1 = <<< STR
<strong>Successful upload/installation</strong><p style='margin:3px'>You will now be able to view the menu on your Joomla website.
<p style='margin:3px'><span style='font-weight: bold; color: #990000;'><br/>If you do not see the menu, please check one of the following:</span>
<ul style='margin-left:15px'>
<li>
<p style='margin:3px'>The "Menu name" value should match the value in the "Compiled Menu Name" property of the AllWebMenus Pro project file (<i>Tools &gt; Project Properties &gt; Folders</i>). <a id="show_me_0" href="javascript:void(0)" onclick="show_awm_folder_info(0);">show me</a></p>
								<div id="AWM_folder_info_0" style="margin-top: 20px; display: none; background-color: #FEFCF5; border: #E6DB55 solid 1px;">
									<table>
										<tr><td style="width: 800px; text-align: center; padding-top: 10px; padding-bottom: 10px;"><strong>More info</strong></td></tr>
										<tr><td style="width: 800px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
											<img src="components/com_allwebmenuspro/images/more_info.jpg" width="527" height="513" alt="More info" title="More info"/>
										</td></tr>
										<tr><td style="width: 800px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
											<a href="javascript:void(0)" onclick="show_awm_folder_info(0);">close</a>
									</table>
								</div>
</li>
<li><p style='margin:3px'>Your menu may use the &quot;Relative to an Element&quot; positioning but you may have not positioned the menu module or assigned it to &quot;all pages&quot;.
<li><p style='margin:3px'>Your menu may use the &quot;Relative to an Element&quot; (or Image) positioning but you may have not specified an available &quot;Element ID&quot; or &quot;Image Name&quot; in the <Positioning> property of AllWebMenus Pro.
<li><p style='margin:3px'>Your menu may use the &quot;Relative to an Element&quot; positioning but you may have not activated and positioned the respective AllWebMenus Pro module in Joomla.
</ul>
STR;


            $mainframe->enqueueMessage( $msg1 );
		} else {
			$mainframe->enqueueMessage( "Almost Successful upload/installation. Please read the errors above." , 'notice' );
		}
	} else {
		$msg =  "There was an error when you tried to upload your AllWebMenus Pro Compiled Menu ZIP File.<br />";
		switch ($error_occured) {
			case 1:
				$msg .= "It looks like you have not selected any file, or the size of the file is zero (0).<br/ >";
				break;
			case 2:
				$msg .= "It looks like you are trying to upload a file which was not created by AllWebMenus Pro Joomla Menu Add-in,<br />or you have renamed the produced ZIP file.<br/ >";
				$msg .= "<br />Try to upload only a ZIP file that has been created by the AllWebMenus Pro Joomla Menu Add-in,<br />and do not change its filename, otherwise you may have problems.<br/ >";
				break;
			case 3:
				$msg .= "It looks like you are trying to upload a file which was not created by AllWebMenus Pro Joomla Menu Add-in,<br />or you have renamed the produced ZIP file.<br/ >";
				$msg .= "<br />Try to upload only a ZIP file that has been created by the AllWebMenus Pro Joomla Menu Add-in,<br />and do not change its filename, otherwise you may have problems.<br/ >";
				break;
		}
		$msg .= '<br />Please <strong><a href="<?php echo JURI::base(); ?>/index.php?option=com_allwebmenuspro&controller=awmupload">try again</a></strong>.';
		$mainframe->enqueueMessage( $msg , 'error' );
	}
}

public static function unpack($p_filename) {
		$archivename = $p_filename;
		$tmpdir = uniqid('install_');
		$extractdir = JPath::clean(dirname($p_filename).DS.$tmpdir);
		$archivename = JPath::clean($archivename);
		$result = HTML_allwebmenuspro::extract( $archivename, $extractdir);
		if ( $result === false ) {
			return false;
		} else {
			$retval['extractdir'] = $extractdir;
			$retval['packagefile'] = $archivename;
			$retval['dir'] = $extractdir;
			return $retval;
		}
}

public static function cleanupInstall($package, $resultdir)
{
		$config = JFactory::getConfig();
		if (is_dir($resultdir)) {
			JFolder::delete($resultdir);
		}
		if (is_file($package)) {
			JFile::delete($package);
		} elseif (is_file(JPath::clean($config->get('config.tmp_path').DS.$package))) {
			JFile::delete(JPath::clean($config->get('config.tmp_path').DS.$package));
		}
}

public static function extract( $archivename, $extractdir)
{
		$result = false;
		$adapter = HTML_allwebmenuspro::getAdapter('zip');
		if ($adapter) {
			$result = $adapter->extract($archivename, $extractdir);
		}
		if (! $result || JError::isError($result)) {
			return false;
		}
		return true;
}

public static function &getAdapter($type)
{
		static $adapters;
		if (!isset($adapters)) {
			$adapters = array();
		}
		if (!isset($adapters[$type]))
		{
			$class = 'JArchiveZipAwm';
			if (!class_exists($class))
			{
				$path = dirname(__FILE__).DS.'library'.DS.'zipawm.php';
				if (file_exists($path)) {
					require_once($path);
				} else {
					JError::raiseError(500,JText::_('Unable to load archive'));
				}
			}
			$adapters[$type] = new $class();
		}
		return $adapters[$type];
}

}
?>
